﻿// Start.cpp : Defines the entry point for the application.
//

#include "Start.h"
#include <cpprest/http_client.h>
#include <cpprest/filestream.h>
#include <cpprest/http_listener.h>              // HTTP server
#include <cpprest/json.h>                       // JSON library
#include <cpprest/uri.h>                        // URI library
#include <cpprest/ws_client.h>                  // WebSocket client
#include <cpprest/containerstream.h>            // Async streams backed by STL containers
#include <cpprest/interopstream.h>              // Bridges for integrating Async streams with STL and WinRT streams
#include <cpprest/rawptrstream.h>               // Async streams backed by raw pointer to memory
#include <cpprest/producerconsumerstream.h>     // Async streams for producer consumer scenarios
#include <string.h>
#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_utils.hpp"  //rapidxml::file
#include "rapidxml/rapidxml_print.hpp"  //rapidxml::print
#include <sqlite3.h> 
#include "OrderRequest.h"
#include "Repository.h"
#include "CommandHandler.h"

using namespace utility;                    // Common utilities like string conversions
using namespace web;                        // Common features like URIs.
using namespace web::http;                  // Common HTTP functionality
using namespace web::http::client;          // HTTP client features
using namespace concurrency::streams;       // Asynchronous streams
using namespace std;
using namespace web::http::experimental::listener;          // HTTP server
using namespace web::json;                                  // JSON library
using namespace web::details;

#define listener "https://*:8080";
#define database "D:/Source/Repos/WeiXinPublic/CMakeProject1/CMakeProject1/Pro.db";

int main(int argc, char* argv[])
{
        // Start server.
        cout << "Starting Server" << endl;

        try {
            utility::string_t address = U(listener);
            uri_builder uri(address);
            auto addr = uri.to_uri().to_string();
            CommandHandler handler(addr);
            handler.open().wait();

            ucout << utility::string_t(U("Listening for requests at: ")) << addr << std::endl;
            ucout << U("Press ENTER key to quit...") << std::endl;
            std::string line;
            std::getline(std::cin, line);
            handler.close().wait();

        }
        catch (exception& e) {
            cout << "--- ERROR DETECTED ---" << endl;
            cout << e.what() << endl;
            ucout << U("Press ENTER key to quit...") << std::endl;
            std::string line;
            std::getline(std::cin, line);

        }

        return 0;
}
