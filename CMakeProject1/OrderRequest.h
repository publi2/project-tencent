#ifndef SHAPE_H
#define SHAPE_H

#include <string>
#include <iostream>
#include <stdio.h>
#include <rapidxml/rapidxml.hpp>

using namespace std;

class OrderRequest {
	public:
		OrderRequest();
		OrderRequest(rapidxml::xml_node<>* root);
		string AppId;
		string MchId;
		string TransactionId;
		string OutTradeNo;
		string NonceStr;
		string Sign;
};

#endif