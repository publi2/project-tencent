#include "pch.h"

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    RUN_ALL_TESTS();
}

TEST(TestCaseName, TestName) {
  EXPECT_EQ(1, 1);
  EXPECT_TRUE(true);
}

TEST(simpleTest, test1) {
    float base = 4.f;
    float exponent = 1.f;
    float expectedValue = 4.f;
    float actualValue = pow(base, exponent);
    EXPECT_FLOAT_EQ(expectedValue, actualValue);
}
TEST(simpleTest, test2) {
    float base = 4.f;
    float exponent = 2.f;
    float expectedValue = 16.f;
    float actualValue = pow(base, exponent);
    EXPECT_FLOAT_EQ(expectedValue, actualValue);
}
