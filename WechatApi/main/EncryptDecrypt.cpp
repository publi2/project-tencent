#include <iostream>

using namespace std;

/// <summary>
/// The function to encrypt and decrypt a string.
/// </summary>
/// <param name="toEncrypt">The string to be encrypted and decrypted.</param>
/// <returns>results.</returns>
string encryptDecrypt(string toEncrypt) {
    char key = 'K'; //Any char will work
    string output = toEncrypt;

    for (int i = 0; i < toEncrypt.size(); i++)
        output[i] = toEncrypt[i] ^ key;

    return output;
}

