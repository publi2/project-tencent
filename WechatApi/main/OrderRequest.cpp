// OrderRequest.cpp : Defines the order query request.

#include <string>
#include <iostream>
#include <stdio.h>
#include <rapidxml/rapidxml.hpp>
#include "OrderRequest.h"

using namespace std;

/// <summary>
/// Order request object.
/// </summary>
/// <param name="root">The XML root for object to initial.</param>
OrderRequest::OrderRequest(rapidxml::xml_node<>* root) {
    rapidxml::xml_node<>* node_first = root->first_node("appid");

    if (node_first != NULL) {
        cout << "attr_name:" << node_first->name() << endl;
        cout << "attr_value:" << node_first->value() << endl;
        AppId = node_first->value();
    }

    node_first = root->first_node("mch_id");
    if (node_first != NULL) {
        cout << "attr_name:" << node_first->name() << endl;
        cout << "attr_value:" << node_first->value() << endl;
        MchId = node_first->value();
    }

    node_first = root->first_node("transaction_id");
    if (node_first != NULL) {
        cout << "attr_name:" << node_first->name() << endl;
        cout << "attr_value:" << node_first->value() << endl;
        TransactionId = node_first->value();
    }

    node_first = root->first_node("out_trade_no");
    if (node_first != NULL) {
        cout << "attr_name:" << node_first->name() << endl;
        cout << "attr_value:" << node_first->value() << endl;
        OutTradeNo = node_first->value();
    }

    node_first = root->first_node("nonce_str");
    if (node_first != NULL) {
        cout << "attr_name:" << node_first->name() << endl;
        cout << "attr_value:" << node_first->value() << endl;
        NonceStr = node_first->value();
    }
    
    if (node_first != NULL) {
        node_first = root->first_node("sign");
        cout << "attr_name:" << node_first->name() << endl;
        cout << "attr_value:" << node_first->value() << endl;
        Sign = node_first->value();
    }
}
