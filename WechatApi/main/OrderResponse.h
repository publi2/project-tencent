#ifndef SHAPE_R
#define SHAPE_R

#include <string>
#include <iostream>
#include <stdio.h>
#include <rapidxml/rapidxml.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/binary_object.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/nvp.hpp>

using namespace std;
using boost::serialization::make_nvp;

class OrderResponse {
private:
	friend class boost::serialization::access;

	template<class Archive>
	void serialize(Archive& ar, const unsigned int version) {

		ar& make_nvp("appid", AppId);
		ar& make_nvp("mch_id", MchId);
		ar& make_nvp("transaction_id", TransactionId);
		ar& make_nvp("out_trade_no", OutTradeNo);
		ar& make_nvp("nonce_str", NonceStr);
		ar& make_nvp("sign", Sign);
		ar& make_nvp("return_code", ResultCode);
		ar& make_nvp("err_code", ErrorCode);
		ar& make_nvp("err_code_des", ErrorCodedes);
		ar& make_nvp("device_info", Device_Info);
		ar& make_nvp("is_subscribe", IsSubscribe);
		ar& make_nvp("trade_type", TradeType);
		ar& make_nvp("trade_state", TradeState);
		ar& make_nvp("bank_type", BankType);
		ar& make_nvp("total_fee", TotalFee);
		ar& make_nvp("fee_type", FeeType);
		ar& make_nvp("cash_fee", CashFee);
		ar& make_nvp("cash_fee_type", CashFeeType);
		ar& make_nvp("coupon_fee", CouponFee);
		ar& make_nvp("coupon_count", CouponCount);
		ar& make_nvp("coupon_id_n", CouponIdN);
		ar& make_nvp("coupon_fee_n", CouponFeeN);
		ar& make_nvp("attach", Attach);
		ar& make_nvp("time_end", TimeEnd);
		ar& make_nvp("trade_state_desc", TradeStateDesc);
	}

public:
	OrderResponse() {};
	void InitializeValue(int argc, char** argv, char** azColName);
	void WriteToStreamInXML() {};

	string AppId;
	string MchId;
	string TransactionId;
	string OutTradeNo;
	string NonceStr;
	string Sign;
	string ResultCode;
	string ErrorCode;
	string ErrorCodedes;
	string Device_Info;
	string OpenId;
	string IsSubscribe;
	string TradeType;
	string TradeState;
	string BankType;
	int TotalFee;
	string FeeType;
	int CashFee;
	string CashFeeType;
	int CouponFee;
	int CouponCount;
	string CouponIdN;
	int CouponFeeN;
	string Attach;
	string TimeEnd;
	string TradeStateDesc;
};

#endif