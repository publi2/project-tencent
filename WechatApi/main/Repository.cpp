#include <sqlite3.h> 
#include <stdio.h>
#include <tchar.h>
#include "Repository.h"
#include <string>

/// <summary>
/// The callback function for SQLite API to call.
/// It's called when for every record the SQL query.
/// </summary>
/// <param name="data">The data passed in for callback function to be handled.</param>
/// <param name="argc">The col count.</param>
/// <param name="argv">The value array.</param>
/// <param name="azColName">The col name value.</param>
/// <returns>The int to indicate the result.</returns>
static int callback(void* data, int argc, char** argv, char** azColName) {

    // int argc: holds the number of results
    // (array) azColName: holds each column returned
    // (array) argv: holds each value
    fprintf(stderr, "%s: ", (const char*)data);
    for (int i = 0; i < argc; i++) {
        // Show column name, value, and newline
        cout << azColName[i] << ": " << argv[i] << endl;
    }

    OrderResponse* response = (OrderResponse*)data;
    response->InitializeValue(argc, argv, azColName);

    // Insert a newline
    cout << endl;

    // Return successful
    return 0;
}

Repository::Repository(const string dbNamePara) {
    dbName = dbNamePara;
}

/// <summary>
/// Connect to repository.
/// </summary>
void Repository::Connect() {
    rc = sqlite3_open(dbName.data(), &db);

    if (rc) {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        exit(0);
    }
    else {
        fprintf(stderr, "Opened database successfully\n");
    }
}

/// <summary>
/// Close the repository.
/// </summary>
void Repository::Close() {
    sqlite3_close(db);
}

/// <summary>
/// Get the order by order request.
/// </summary>
/// <param name="orderRequest">The order request to passed in.</param>
/// <returns>The OrderResponse found.</returns>
OrderResponse Repository::Get_Order_By_Id(OrderRequest orderRequest) {
    // get order by transaction_id or out_trade_no.
    // transaction_id is the first priority.

    string sql;
    if (orderRequest.TransactionId.empty()) {

        string tmp = "';";
        sql = "SELECT * FROM 'ORDERTABLE' WHERE out_trade_no = '" + orderRequest.OutTradeNo + tmp;
    }
    else {
        
        string tmp = "';";
        sql = "SELECT * FROM 'ORDERTABLE' WHERE transaction_id = '" + orderRequest.TransactionId + tmp;
    }

    char* zErrMsg = 0;
    const char* data = "Callback function called";
    OrderResponse response;
    rc = sqlite3_exec(db, sql.c_str(), callback, &response, &zErrMsg);

    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else {
        fprintf(stdout, "Operation done successfully\n");
    }
    return response;
}
