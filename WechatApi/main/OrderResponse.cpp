#include <string>
#include <iostream>
#include <stdio.h>
#include <rapidxml/rapidxml.hpp>
#include "OrderResponse.h"
#include "EncryptDecrypt.cpp"

/// <summary>
/// For string to int hash, this is for string switch as we could only switch for Enum or int.
/// </summary>
/// <param name="str">The string to be converted.</param>
/// <param name="h">The index int for hash. default is 0.</param>
/// <returns>The hashed number.</returns>
constexpr unsigned int str2int(const char* str, int h = 0)
{
    return !str[h] ? 5381 : (str2int(str, h + 1) * 33) ^ str[h];
}

void OrderRequest() {}

void OrderResponse::InitializeValue(int argc, char** argv, char** azColName) {
    for (int i = 0; i < argc; i++) {
        // Show column name, value, and newline
        cout << azColName[i] << ": " << argv[i] << endl;
        switch (str2int(azColName[i])) {
        case str2int("appid"):
            AppId = argv[i];
            break;
        case str2int("mchid"):
            MchId = argv[i];
            break;
        case str2int("noncestr"):
            NonceStr = argv[i];
            break;
        case str2int("sign"):
            Sign = argv[i];
            break;
        case str2int("resultcode"):
            ResultCode = argv[i];
            break;
        case str2int("errorcode"):
            ErrorCode = argv[i];
            break;
        case str2int("errorcodedes"):
            ErrorCodedes = argv[i];
            break;
        case str2int("device_info"):
            Device_Info = argv[i];
            break;
        case str2int("openid"):
            OpenId = encryptDecrypt(argv[i]);
            break;
        case str2int("is_subscribe"):
            IsSubscribe = argv[i];
            break;
        case str2int("trade_type"):
            TradeType = argv[i];
            break;
        case str2int("trade_state"):
            TradeState = argv[i];
            break;
        case str2int("bank_type"):
            BankType = argv[i];
            break;
        case str2int("total_fee"):
            TotalFee = atoi(argv[i]);
            break;
        case str2int("fee_type"):
            FeeType = argv[i];
            break;
        case str2int("cash_fee"):
            CashFee = atoi(argv[i]);
            break;
        case str2int("cash_fee_type"):
            CashFeeType = argv[i];
            break;
        case str2int("coupon_fee"):
            CouponFee = atoi(argv[i]);
            break;
        case str2int("coupon_count"):
            CouponCount = atoi(argv[i]);
            break;
        case str2int("coupon_id_n"):
            CouponIdN = argv[i];
            break;
        case str2int("coupon_fee_n"):
            CouponFeeN = atoi(argv[i]);
            break;
        case str2int("transaction_id"):
            TransactionId = argv[i];
            break;
        case str2int("out_trade_no"):
            OutTradeNo = argv[i];
            break;
        case str2int("attach"):
            Attach = argv[i];
            break;
        case str2int("time_end"):
            TimeEnd = argv[i];
            break;
        case str2int("trade_state_desc"):
            TradeStateDesc = argv[i];
            break;
        }


    }
}
