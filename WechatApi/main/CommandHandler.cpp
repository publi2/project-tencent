#include "Start.h"
#include <cpprest/http_client.h>
#include <cpprest/filestream.h>
#include <cpprest/http_listener.h>              // HTTP server
#include <cpprest/json.h>                       // JSON library
#include <cpprest/uri.h>                        // URI library
#include <cpprest/ws_client.h>                  // WebSocket client
#include <cpprest/containerstream.h>            // Async streams backed by STL containers
#include <cpprest/interopstream.h>              // Bridges for integrating Async streams with STL and WinRT streams
#include <cpprest/rawptrstream.h>               // Async streams backed by raw pointer to memory
#include <cpprest/producerconsumerstream.h>     // Async streams for producer consumer scenarios
#include <string.h>
#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_utils.hpp"  //rapidxml::file
#include "rapidxml/rapidxml_print.hpp"  //rapidxml::print
#include <sqlite3.h> 
#define CEREAL_XML_STRING_VALUE "xml"
#include "cereal/archives/binary.hpp"
#include "cereal/types/unordered_map.hpp"
#include "cereal/types/memory.hpp"
#include "cereal/archives/xml.hpp"
#include "cereal/types/string.hpp"
#include <fstream>
#include <strstream>
#include "OrderRequest.h"
#include "Repository.h"
#include "CommandHandler.h"
#include "GeneralService.cpp"
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/binary_object.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/export.hpp>
#include <boost/archive/text_oarchive.hpp>

using namespace utility;                    // Common utilities like string conversions
using namespace web;                        // Common features like URIs.
using namespace web::http;                  // Common HTTP functionality
using namespace web::http::client;          // HTTP client features
using namespace concurrency::streams;       // Asynchronous streams
using namespace std;
using namespace web::http::experimental::listener;          // HTTP server
using namespace web::json;                                  // JSON library
using namespace web::details;


/// <summary>
/// The function to compare the RUL
/// </summary>
/// <param name="stringA">String A.</param>
/// <param name="stringB">String B.</param>
/// <returns>True if it equals.</returns>
bool comparei(wstring stringA, wstring stringB)
{
    transform(stringA.begin(), stringA.end(), stringA.begin(), toupper);
    transform(stringB.begin(), stringB.end(), stringB.begin(), toupper);

    return (stringA == stringB);
}

CommandHandler::CommandHandler(utility::string_t url) : m_listener(url)
{
    // We do not support get now.
    // m_listener.support(methods::GET, std::bind(&CommandHandler::handle_get_or_post, this, std::placeholders::_1));
    m_listener.support(methods::POST, std::bind(&CommandHandler::handle_get_or_post, this, std::placeholders::_1));
}

/// <summary>
/// The function to control the API url match.
/// </summary>
/// <param name="message">The http request.</param>
void CommandHandler::handle_get_or_post(http_request message)
{
    ucout << "Method: " << message.method() << std::endl;
    ucout << "URI: " << http::uri::decode(message.relative_uri().path()) << std::endl;
    ucout << "Query: " << http::uri::decode(message.relative_uri().query()) << std::endl << std::endl;
    if (message.method() == methods::POST && comparei(message.relative_uri().path(), L"/pay/paporderquery")) {
        // match the api.
        handle_post_paporderquery(message);
    }
    else {
        message.reply(status_codes::NotImplemented, "NOT SUPPORT YET");
    }
}

/// <summary>
/// Handle API for: POST, /pay/paporderquery
/// </summary>
/// <param name="request">The http request.</param>
void CommandHandler::handle_post_paporderquery(http_request request)
{
    rapidxml::xml_document<> doc;
    std::wstring xmlString = request.extract_string().get().c_str();

    std::string str;
    str.assign(xmlString.begin(), xmlString.end());

    std::vector<char> char_array(str.begin(), str.end());
    char_array.push_back(0);

    doc.parse<0>(&char_array[0]);

    rapidxml::xml_node<>* root = doc.first_node();
    cout << "root:" << root << endl;
    cout << *root << endl;

    OrderRequest orderRequest(root);

    Repository repository("D:/Source/Repos/WeiXinPublic/CMakeProject1/CMakeProject1/Pro.db");

    repository.Connect();
    OrderResponse orderResponse = repository.Get_Order_By_Id(orderRequest);
    repository.Close();

    // the order is not found.
    if (orderResponse.TransactionId.empty() && orderResponse.OutTradeNo.empty()) {
        request.reply(status_codes::NotFound, "Order not found");
        return;
    }
    unsigned int flags = boost::archive::no_header;

    std::stringstream wf;
    boost::archive::xml_oarchive oa(wf, flags);

    oa << BOOST_SERIALIZATION_NVP(orderResponse);

    request.reply(status_codes::OK, wf.str());

    return;
}
