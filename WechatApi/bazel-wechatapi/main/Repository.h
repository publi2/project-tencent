#include <sqlite3.h> 
#include <stdio.h>
#include <tchar.h>
#include "OrderResponse.h"
#include "OrderRequest.h"
class Repository {
	public:
		Repository(const string dbName);
		OrderResponse Get_Order_By_Id(OrderRequest orderRequest);
		void Close();
		void Connect();
	private:
		sqlite3* db;
		int rc;
		string dbName;
};

